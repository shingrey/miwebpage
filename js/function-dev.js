/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 27/05/2015
 * César Flores
 * 
 */

$(document).ready(function(){
    var ww = screen.width;
    var hh = screen.height;
    var w= $(window).outerWidth();
    var h= $(window).outerHeight();
    console.log(hh);
    //alert(window.innerHeight);
    if(ww < 900){
        $("section article section article").css("height",hh);
        $("#uno,#am,#lpz").css("height",hh);
    }    

    
    menu=0;
    $("#menu").click(function(){
        if(menu==0){
            $("nav").css("display", "block");
            $("nav").addClass("animatemenu");
            $("#fmenu").removeClass("menu-1").addClass("menu-2");
            $("#logogif").css("background-color","309D83");
            menu=1;
            
        }
        else{
            $("nav").removeClass("animatemenu");
            $("#fmenu").removeClass("menu-2").addClass("menu-1");
            $("#logogif").css("background-color","");
            $("nav").animate({width: "0%"},400, function(){
                $(this).css("display","");
                $(this).css("width","");
                
            });
            
            menu=0;
        }
       /* 
        $(window).on("scroll", function (){
            $(this).scrollTop($(window).scrollTop()); //sets the scrollposition to 100px
            });
        */
    });
   /*smooth scroll*/
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $("html, body").animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

  
    $(window).scroll(function(){
        /*var scll= $(window).height();
        var porcentaje = ($(window).scrollTop()*100)/scll;*/
        var s0= $("#uno").position();//inicio
        var s1= $("#te").position();//tecnologia
        var s2= $("#am").position();//experiencia
        var s3= $("#str").position();//trabajos
        var s4= $("#lpz").position();//quiensoy
        var s5= $("#gmz").position();//contacto
        var dontoy=$(this).scrollTop();
        /* animaciones graficas*/
        var web=$("#web").position();
        var movil=$("#movil").position();
        var escritorio=$("#escritorio").position();
        var bd=$("#bd").position();
        
        /*if para secciones y menu*/
        if(dontoy > s0.top && dontoy < s1.top || dontoy==s0.top){
            limpianav();
            $("li:nth-child(1) a").addClass("seleccion");
            $("li:nth-child(1) a").append("<img src='image/puntero.gif' width='30' id='puntero' alt='puntero' />");
        }
        if(dontoy > s1.top && dontoy < s2.top || dontoy===s1.top){
            limpianav();
            $("li:nth-child(2) a").addClass("seleccion");
            $("li:nth-child(2) a").append("<img src='image/puntero.gif' width='30' id='puntero' alt='puntero' />");
            
            if(dontoy > (s1.top - 100) && dontoy < movil.top || dontoy==s1.top){/*por articulos*/
            
             $("#web #contenedorG").each(function(){
                 $("#web #contenedorG div").addClass("solosolo");
                 $("#web #contenedorG div").css("display","");
                 $("#web #contenedorG span").addClass("solotex");
             });
            
            }
            if(dontoy > (movil.top - 300) && dontoy < escritorio.top || dontoy===movil.top){
            
             $("#movil #contenedorG").each(function(){
                 $("#movil #contenedorG div").addClass("solosolo");
                 $("#movil #contenedorG div").css("display","");
                 $("#movil #contenedorG span").addClass("solotex");
             });
            
            }
            if(dontoy > (escritorio.top - 300) && dontoy < bd.top || dontoy===escritorio.top){
            
             $("#escritorio #contenedorG").each(function(){
                 $("#escritorio #contenedorG div").addClass("solosolo");
                 $("#escritorio #contenedorG div").css("display","");
                 $("#escritorio #contenedorG span").addClass("solotex");
             });
            
            }
            if(dontoy > (bd.top - 300) && dontoy < s2.top || dontoy===bd.top){
            
             $("#bd #contenedorG").each(function(){
                 $("#bd #contenedorG div").addClass("solosolo");
                 $("#bd #contenedorG div").css("display","");
                 $("#bd #contenedorG span").addClass("solotex");
             });
            
            }
            
            
        }
        if(dontoy > s2.top && dontoy < s3.top || dontoy==s2.top){
            limpianav();
            $("li:nth-child(3) a").addClass("seleccion");
            $("li:nth-child(3) a").append("<img src='image/puntero.gif' width='30' id='puntero' alt='puntero' />");
        }
        if(dontoy > s3.top && dontoy < s4.top || dontoy==s3.top){
            limpianav();
            $("li:nth-child(4) a").addClass("seleccion");
            $("li:nth-child(4) a").append("<img src='image/puntero.gif' width='30' id='puntero' alt='puntero' />");
        }
        if(dontoy > s4.top && dontoy < s5.top || dontoy==s4.top){
             limpianav();
            $("li:nth-child(5) a").addClass("seleccion");
            $("li:nth-child(5) a").append("<img src='image/puntero.gif' width='30' id='puntero' alt='puntero' />");
        }
        if(dontoy > s5.top || dontoy==s5.top){
            limpianav();
            $("li:nth-child(6) a").addClass("seleccion");
            $("li:nth-child(6) a").append("<img src='image/puntero.gif' width='30' id='puntero' alt='puntero' />");
        }
        /*--fin--*/
        
        
        
    });
    
});

function limpianav(){
    $("li:nth-child(1) a").removeClass("seleccion");
    $("li:nth-child(2) a").removeClass("seleccion");
    $("li:nth-child(3) a").removeClass("seleccion");
    $("li:nth-child(4) a").removeClass("seleccion");
    $("li:nth-child(5) a").removeClass("seleccion");
    $("li:nth-child(6) a").removeClass("seleccion");
    $("#puntero").remove();
    
}

$(document).keydown(function(tecla){
                if(tecla.which==27 || tecla.which==8){
                    
                    if(menu==1){
                        $("nav").stop();
                        $("nav").removeClass("animatemenu");
                        $("#fmenu").removeClass("menu-2").addClass("menu-1");
                        $("#logogif").css("background-color","");
                        $("nav").animate({width: "0%"},400, function(){
                            $(this).css("display","");
                            $(this).css("width","");
                        });
                    }
                    
                menu=0;
                }
            });