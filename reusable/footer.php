<?php 
 class Footer{
     
     public function __construct(){
         ?>

<footer id="gmz">
    <div class="espacio" ></div>
    <div id="contacto">
        <header><h2>Contacto</h2></header>
        <div class="espacio"></div>
        <form name="correo" role="form" action="./reusable/enviacorreo.php" method="post">
            <input type="text" placeholder="Nombre  Completo" id="Nombre" name="Nombre" required />
            <input type="tel" placeholder="Teléfono" id="Telefono" name="Telefono" />
            <input type="email" placeholder="Email" id="Correo" name="Correo" required />
            <select id="Asunto" name="Asunto" required >
                <option value="" selected="">Seleccione una opción</option>
                <option value="Cotizar sitio web">Cotizar sitio web</option>
                <option value="Modifica sitio web">Modificar sitio web</option>
                <option value="curso">¡Me gustaría un Curso!</option>
                <option value="SEO">Quiero que mi web aparezca en buscadores (google, yahoo, bing)</option>
                <option value="mantenimiento de compu">Mi Computadora esta lenta</option>
                <option value="contratacion">Me gustaría contratarte para un trabajo</option>
                <option value="otro">Otro</option>
            </select>
            <textarea name="textarea" id="textarea" rows="6" placeholder="Escriba su mensaje" required></textarea>
            <input type="submit" id="Enviar" name="Enviar" >
        </form>

	<div>
            <span><em>Si te gusto compartelo en las redes sociales:</em></span>
            <div id="social">
                <a id="fb"><div id="face"></div></a>
                <a id="tw"><div id="twitter"></div></a>
            </div>
        </div>
    </div>


    <script src="js/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="js/function.js" type="text/javascript" defer></script>
    <script>
        var urll= location.href;
        var titulop= document.title;
        window.onload= function (){
            document.getElementById("fb").onclick=function(){
                window.open('https://www.facebook.com/sharer/sharer.php?u='+urll,"compartelo en FB :)","directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=480, height=400");
            };
            document.getElementById("tw").onclick=function(){
                window.open("https://twitter.com/intent/tweet?text="+titulop+"&url="+urll+"&hashtags=DevTux,DesarrollowebTuxtla&via=devwebcf&related=devwebcf%3ADesarrollador%20web,casa_lum%3AHotel%20Boutique%20en%20San%20Cristobal%20de%20las%20Casas","compartelo en Twitter :)","directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=480, height=400");
            };
        };
    </script>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61656416-3', 'auto');
  ga('send', 'pageview');
// ga('require', 'linkid', 'linkid.js'); ga('set', '&uid', {{cf_0}});
</script>
</body>
</html>

<?php
     //fin de la funcion 
     }
     
 }
?>