<?php
require_once './reusable/Graficas.php';
class Section{
    var $barras;
    function presentacion() {
        ?>
        <section>
	    <h2 style="display:none;">Programador web, Desarrollador web, Creación de páginas web, posicionamiento</h2>
	    <br />
            <article>
                <div id="uno">
                    <video width="100%" autoplay loop muted poster="image/home.jpg">
                    <source src="video/corto.webm" type="video/webm">
                    <source src="video/corto.mp4" type="video/mp4">
                    <picture>
                        <source srcset="image/home2k.jpg" media="(min-width: 1981px)">
                        <source srcset="image/home1080.jpg" media="(max-width: 1920px) and (min-width: 1400px)">
                        <source srcset="image/home720.jpg" media="(max-width: 1399px) and (min-width: 961px)">
                        <source srcset="image/home_land.jpg" media="(min-width: 960px) and (orientation: landscape)">
                        <source srcset="image/home.jpg" media="(min-width: 960px) and (orientation: portrait)">
                        <img src="image/home720.jpg" width="100%" height="100%" alt="Equipo de trabajo">
                    </picture>
                    </video>                    
                <div id="mensaje">
                <h1>César Flores - Desarrollador Web</h1>
                </div>
                </div>
            </article>
            
            <article id="te">
                <div class="espacio"></div>
                <header>
                    <h2>Dominio de Tecnologías</h2>
                </header>
                <blockquote>Cada cliente busca lo mejor para su <strong>sitio web</strong>, y se preocupa por que se vea bien y funcione mejor, por ese motivo cada <strong>desarrollo web</strong> se tiene requerimientos distintos, la tecnología debe variar para que se adapte a lo que el usuario final necesite.</blockquote>
                <section>
		    <h2 style="margin-top: 5px; margin-bottom:10px;">Páginas web, Desarrollo Movil y Programación para PC</h2>
                    <article id="web">
                        <header><h3>Manejo de Tecnologías</h3></header>
                        <h4>Web</h4>
                        <blockquote>Cada día es mas importante las empresas tengan una <strong>página web</strong>y también <strong>posicionamiento web</strong>, esto significa un mayor crecimiento del producto o empresa. las opciones son variadas desde una página informativa hasta una tienda online u otras aplicaciones pueden ser opción para mejorar su negocio.</blockquote>
                        <div class="espacio"></div>
                        <?php 
                        //nombre es el data-nombre, color es color de barra, porcentaje = largo, delay= animacion, size = ancho
                            $graficandoando = new Graficas();
                            $barras=array();
                            array_push($barras,array('nombre'=>"H T M L 5",'color'=>"FF7F50",'porcentaje'=>"90",'delay'=>"0",'size'=>"12%"),
                                            array('nombre'=>"C S S 3",'color'=>"84DFFB",'porcentaje'=>"90",'delay'=>"0.1",'size'=>"12%"),
                                            array('nombre'=>"J query",'color'=>"4B92CC",'porcentaje'=>"85",'delay'=>"0.2",'size'=>"12%"),
                                            array('nombre'=>"P H P",'color'=>"8892BF",'porcentaje'=>"75",'delay'=>"0.3",'size'=>"12%"),
                                            array('nombre'=>"X M L",'color'=>"9CAF2D",'porcentaje'=>"75",'delay'=>"0.4",'size'=>"12%"),
                                            array('nombre'=>"J S O N",'color'=>"D3DD87",'porcentaje'=>"80",'delay'=>"0.5",'size'=>"12%"),
                                            array('nombre'=>". N E T",'color'=>"00C4FF",'porcentaje'=>"40",'delay'=>"0.6",'size'=>"12%"));
                            $graficandoando->graficar($barras);
                        ?>
                    </article>
                    <article id="movil">
                        <header><h3>Manejo de Tecnologías</h3></header>
                        <h4>Dispositivos Movil</h4>
                        <blockquote>Si busca diversidad tecnologica, inovación o simplemente complementar su producto, existen alternativas para una mejor integración o experiencia</blockquote>
                        <div class="espacio"></div>
                        <?php 
                            $graficandoando = new Graficas();
                            unset($barras);
                            $barras=array();
                            array_push($barras,array('nombre'=>"Arduino",'color'=>"00929F",'porcentaje'=>"70",'delay'=>"0",'size'=>"40%"),
                                            array('nombre'=>"Android",'color'=>"37B597",'porcentaje'=>"60",'delay'=>"0.1",'size'=>"40%"));
                            $graficandoando->graficar($barras);
                        ?>
                    </article>
                    <article id="escritorio">
                        <!--<div class="espacio"></div>-->
                        <header><h3>Manejo de Tecnologías</h3></header>
                        <h4>Escritorio</h4>
                        <blockquote>Algunas ocaciones y proyectos no son dirigidos a <strong>servicios web</strong>, en estos casos existen otras alternativa, ¡esa computadora puede hacer más cosas sin necesitad de internet!</blockquote>
                        <div class="espacio"></div>
                        <?php 
                            $graficandoando = new Graficas();
                            unset($barras);
                            $barras=array();
                            array_push($barras,array('nombre'=>"Java",'color'=>"EA0001",'porcentaje'=>"50",'delay'=>"0",'size'=>"30%"),
                                            array('nombre'=>"C #",'color'=>"17B148",'porcentaje'=>"30",'delay'=>"0.1",'size'=>"30%"),
                                            array('nombre'=>"Flex",'color'=>"C6C4C5",'porcentaje'=>"30",'delay'=>"0.2",'size'=>"30%"));
                            $graficandoando->graficar($barras);
                        ?>
                    </article>
                    <article id="bd">
                        <!--<div class="espacio"></div>-->
                        <header><h3>Manejo de Tecnologías</h3></header>
                        <h4>Base de datos</h4>
                        <blockquote>Mantener Los productos, datos y estadisticas siempre al día es una tarea que se usa para cualquier desarrollo de buena proporción</blockquote>
                        <div class="espacio"></div>
                        <?php 
                            $graficandoando = new Graficas();
                            unset($barras);
                            $barras=array();
                            array_push($barras, array('nombre'=>"Mysql",'color'=>"015a84",'porcentaje'=>"80",'delay'=>"0",'size'=>"40%"),
                                            array('nombre'=>"SQL",'color'=>"BA141A",'porcentaje'=>"50",'delay'=>"0.1",'size'=>"40%"));
                            $graficandoando->graficar($barras);
                        ?>
                    </article>
                </section>
            </article>
            <article id="am">
                <div class="espacio"></div>
                <header>
                    <h2>Experiencia laboral</h2>
                </header>
                <div class="espacio"></div>
                <table>
                <thead>
                    <tr>
                        <th>Año</th>
                        <th>Empresa</th>
                        <th>Tiempo </th>
                        <th>Experiencia</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>2013</td>
                        <td>CAM<br/><img src="image/cam.png" alt="Centro de atención Multiple (C.A.M)"/></td>
                        <td>6 Meses</td>
                        <td>Android, Arduino, Circuitos</td>
                    </tr>
                    <tr>
                        <td>2013</td>
                        <td>TG Software<br/> <img src="image/tg.png" alt="Tuxtla Gtz Software"/></td>
                        <td>6 Meses</td>
                        <td>Android, PHP, Js, JSON, XML</td>
                    </tr>
                    <tr>
                        <td>2014</td>
                        <td>Induxsoft<br/><img src="image/induxosf.gif" alt="Induxsoft"/></td>
                        <td>3 Meses</td>
                        <td>.NET, SQL, C#</td>
                    </tr>
                    <tr>
                        <td>2014-Actual</td>
                        <td>COPICO<br/><img src="image/copico.png" alt="Comercializadora de pinturas y complementos (COPICO)"/></td>
                        <td>1 año</td>
                        <td>PHP, HTML, CSS, JS, XML, Flex, Seo, Webmaster</td>
                    </tr>
                </tbody>
                
            </table>
            </article>
            <article id="str">
                <div class="espacio"></div>
                <header>
                    <h2>Portafolio</h2>
                </header>
                <div class="proyectohecho">
                    <div class="proyectosweb" id="proyecto-1" style="background-image: url(image/portafolio/nomicloud/nomicloud-home.jpg);" title="Nomicloud"><span><a href="http://www.aprosi.com.mx" target="_blank" rel="nofollow">Nomicloud</a> <br/>(Reportes, Correcciones)</span></div>
                <div class="proyectosweb" id="proyecto-2" style="background-image: url(image/portafolio/Copico/copico-home.jpg);" title="Grupo Copico"><span><a href="http://grupocopico.com/" target="_blank">Grupo Copico</a> <br/>(Desarrollo, SEO)</span></div>
                <div class="proyectosweb" id="proyecto-3" style="background-image: url(image/portafolio/casa-lum/casa-lum-home.jpg);" title="Aprosi"><span><a href="http://casalum.com/" target="_blank">Hotel Casa Lum</a> <br/>(Desarrollo, SEO)</span></div>
                <div class="proyectosweb" id="proyecto-4" style="background-image: url(image/portafolio/aprosi/aprosi-home.jpg);" title="Casa lum"><span><a href="http://www.aprosi.com.mx" target="_blank">Aprosi</a> <br/>(Front End, SEO)</span></div>
                </div>
            </article>
            <article id="lpz">
                <div class="quiens">
                <header>
                    <h2>¿Quien Soy?</h2>
                </header>
                <div class="contienea"><div id="cesar"></div></div>
                <div id="descripcion">
                    <ul >
                        <li>Profesión: <strong>Desarrollador web</strong>/<strong>Programador web</strong></li>
                        <li>Carrera: Ing. Sistemas computacionales</li>
                        <li>Localidad: Tuxtla Gutiérrez, Chiapas, México</li>
                        <li>Experiencia: Programación Web, Android, Arduino, <strong>Posicionamiento de páginas web</strong>, Webmaster</li>
                        <li>Mi nombre es : César E. Flores Ramón.</li>
                        <li><strong>César Flores</strong></li>
                    </ul>
                </div>
                </div>
            </article>
            
</section>
<?php
    }
}