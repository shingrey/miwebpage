<?php 
 class Head{
     function __construct(){
         
         ?>


<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, height=device-height">
        <link href="favicon-32x32.png" rel="icon" type="image/png" sizes="32x32">
        <link href="favicon-96x96.png" rel="icon" type="image/png" sizes="96x96">
        <link rel="apple-touch-icon" href="favicon-96x96.png">
        <link href="favicon.ico" rel="icon" type="image/x-icon" />
        <title>Desarrollo web y diseño web - César Flores</title>
        <link href="css/mrflower.css" type="text/css" rel="stylesheet" />
        <link href="css/Grafica.css" type="text/css" rel="stylesheet" />
        <meta name="description" content="Programador web, posicionamiento web, Manejo de nuevas tecnologías emergentes, manejo de back end y front end en proyectos, Carrera en Ing. en sistemas computacionales">
        <link rel="canonical" href="http://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
        <link rel="publisher" href="https://plus.google.com/u/0/116289161492323006119/posts">
        <meta property="og:locale" content="es_ES">
        <meta property="og:type" content="website">
        <meta property="og:title" content="César Flores - Programador web">
        <meta property="og:description" content="Programador web, posicionamiento web, Manejo de nuevas tecnologías emergentes, manejo de back end y front end en proyectos, Carrera en Ing. en sistemas computacionales">
        <meta property="og:url" content="http://cesarflores.xyz/">
        <meta property="og:site_name" content="César Flores - Programador web">
        <meta property="article:publisher" content="https://www.facebook.com/shingrey1">
        <meta property="og:image" content="./image/logo2.jpg">
        <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "WebPage",
          "breadcrumb": "Home > Contacto",
          "author": "https://www.facebook.com/shingrey1",
          "about": "Programador web, posicionamiento web, Manejo de nuevas tecnologías emergentes, manejo de back end y front end en proyectos, Carrera en Ing. en sistemas computacionales."
          "telephone": "(961) 255 1703",
          "address": {
                "@type": "PostalAddress",
                "addressLocality": "Tuxtla Gutierrez",
                "addressRegion": "Chiapas, México",
                "postalCode": "29096",
                "streetAddress": "Ave. Flamingos 170 Col. los pajaros."
              },
        }
        </script>
        
        
    </head>
    <body>
        <div id="barra">
            <div id="menu"><div id="fmenu" class="menu-1"></div></div>
            <header class="logo"><h2>César Flores</h2><img src="image/logo.gif" class="logogif" id="logogif" alt="César Flores Programador Web"></header>
        <nav>
            <ul>
                <li><a href="#uno" class="seleccion">Inicio<img src="image/puntero.gif" width="30" id="puntero" alt="puntero"></a></li>
                <li><a href="#te">Tecnologías</a></li>
                <li><a href="#am">Experiencia</a></li>
                <li><a href="#str">Trabajos</a></li>
                <li><a href="#lpz">¿Quien soy?</a></li>
                <li><a href="#gmz">Contacto</a></li>
            </ul>
        </nav>
        </div>
        
<?php

     }
     
 }
?>
